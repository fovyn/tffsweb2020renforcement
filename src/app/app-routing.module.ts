import { Page2Component } from './components/page2/page2.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
  {path: 'page2', component: Page2Component}, // l'url correspond à http://localhost:4200/page2 => affiche dans le router-outlet le composant Page2Component
  { path: 'contact', loadChildren: () => import('./modules/contact/contact.module').then(m => m.ContactModule)},
  { path: 'produit', loadChildren: () => import('./modules/produit/produit.module').then(m => m.ProduitModule) }, //Toutes les routes du module Contact commenceront par contact/... Autre chose, loadChildren permet lui de faire du LazyLoading
  //LazyLoading => Charge le module contact que si je vais sur la page contact
  // l'URL correspond à http://localhost:4200/contact/ => affiche dans le router-outlet le composant correspondant un '' dans le module ContactModule
  // L'URL correspond à http://localhost:4200/contact/form => afficher le composant ContactFormComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
