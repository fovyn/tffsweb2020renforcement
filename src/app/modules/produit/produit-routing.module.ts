import { PanierComponent } from './components/panier/panier.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProduitComponent } from './components/produit/produit.component';

const routes: Routes = [
  { path: '', component: ProduitComponent },
  { path: 'panier', component: PanierComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduitRoutingModule { }
