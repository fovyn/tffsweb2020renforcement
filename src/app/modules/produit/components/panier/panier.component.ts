import { PanierService } from './../../services/panier.service';
import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  constructor(private panierService: PanierService) { }

  ngOnInit(): void {
    
  }

  get Products(): Array<{title: string, price: number}> {
    console.log(this.panierService);
    return this.panierService.getAll();
  }

  onRemoveAction(title: string) {
    this.panierService.remove(title);
  }

  getNbItem(product: {title: string, price: number}): number {
    return this.panierService.getNbItem(product);
  }

  getTotalPrice(product: {title: string, price: number}): number {
    return this.getNbItem(product) * product.price;
  }
}
