import { PanierService } from './../../services/panier.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.scss']
})
export class ProduitComponent implements OnInit {
  
  products: Array<{title: string, price: number}> = [];

  constructor(private panierService: PanierService) { }

  ngOnInit(): void {
    this.products.push({title: "Produit 1", price: 42.42});
    this.products.push({title: "Produit 2", price: 42.48});
    this.products.push({title: "Produit 3", price: 42.44});
    this.products.push({title: "Produit 4", price: 42.45});
    this.products.push({title: "Produit 5", price: 42.46});
    this.products.push({title: "Produit 6", price: 42.47});
    this.products.push({title: "Produit 7", price: 42.48});
    this.products.push({title: "Produit 8", price: 42.49});
    this.products.push({title: "Produit 9", price: 42.41});
    this.products.push({title: "Produit 10", price: 42.40});
  }

  onBuyAction(produit: {title: string, price: number}) {
    this.panierService.add(produit);
  }

}
