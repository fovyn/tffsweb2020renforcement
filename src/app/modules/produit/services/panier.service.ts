import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  private products: Map<{title: string, price: number}, number> = new Map<{title: string, price: number}, number>();
  constructor() { }


  getAll(): Array<{title: string, price: number}> {
    return [...this.products.keys()];
  }

  getNbItem(product: {title: string, price: number}) {
    return this.products.get(product);
  }

  add(product: {title: string, price: number}) {
    if (this.products.has(product)) {
      this.products.set(product, this.products.get(product) + 1)
    } else {
      this.products.set(product, 1)
    }
  }

  remove(title: string) {
    const product = this.getAll().find(p => p.title === title);

    this.products.delete(product);
  }


}
