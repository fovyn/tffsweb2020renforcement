import { CONTACT_FORM } from './../../forms/contact.form';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  //contactForm = nom de l'Attribut de la class ContactFormComponent
  //: FormGroup = Le type de donnée de la variable
  contactForm: FormGroup; // FormGroup = Group de validation de champs de formulaire

  constructor(private builder: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.builder.group(CONTACT_FORM);

    //Déclarer une variable => let/const/var dans une fonction
    //Dans une classe, les attributs de cette classe sont une propriété de celle-ci et par conséquent, sont globals au variable défini dans les fonctions

  }

  /**
   * Method de gestion de la soumission de mon formulaire
   */
  onSubmitAction() {
    if (this.contactForm.valid) {
      console.log("Formulaire valid");      
    } else {
      console.log("Formulaire Invalid");
    }
  }

}
