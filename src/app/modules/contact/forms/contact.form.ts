import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';

//Structure de mon formulaire
export const CONTACT_FORM: {[key: string]: AbstractControl} = {
    email: new FormControl(null, [Validators.required, Validators.email]),
    sujet: new FormControl(null, [Validators.required]),
    message: new FormControl(null, [Validators.required]),
    dateContact: new FormControl(new Date(), [])
}