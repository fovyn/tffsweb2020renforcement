import { AppService } from './../../services/app.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() links: Array<string> = [];
  @Output('onSelectLink') onSelectLinkEvent = new EventEmitter<string>();

  constructor(private appService: AppService) { }

  ngOnInit(): void {
  }

  onLinkClicked(link: string) {
    this.onSelectLinkEvent.emit(link);
    this.appService.link = link;
  }

}
