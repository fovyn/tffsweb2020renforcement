import { routes } from './../../app-routing.module';
import { AppService } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'TFFSWeb2020Renforcement';

  constructor(public appService: AppService) {}

  linksInApp = [];

  ngOnInit() {
    routes.forEach((route: Route) => this.linksInApp.push(route.path));
    this.linksInApp.push("produit/panier");
  }
  onSelectLinkAction(link: string) {
    console.log(link);
  }
}
